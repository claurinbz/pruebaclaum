import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert';

const CrearCuenta = () => {
    const [usuario, setUsuario] = useState({
        nombres: '',
        email: '',
        password: '',
        confirmar: '',
    });

    const { nombres, email, password, confirmar } = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })

    }
    //esta parte es para que aparezca el cursor del mouse directamente en la caja de texto nombres
    useEffect(() => {
        document.getElementById("nombres").focus();
    }, [])

    const crearCuenta = async () => {

        if (password !== confirmar) {
            const msg = "Las Contraseñas son diferentes";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true

                    }
                }
            });
        } else if (password.length < 6) {
            const msg = "La contraseña debe ser de 6 caracteres";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });


        } else {
            const data = {
                nombres: usuario.nombres,
                email: usuario.email,
                password: usuario.password
            }
            const response = await APIInvoke.invokePOST(`/api/usuarios`, data); // api/usuarios es la direccion que esta en el insomnia ejemplo localhost4000/api/usuario
            const mensaje = response.msg;

            if (mensaje === 'El usuario ya existe') { //aqui hay que validar el mensaje como lo tengan en el back
                const msg = "El usuario ya existe";
                swal({
                    title: 'Error',
                    text: msg,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true

                        }
                    }
                });
            }else{
                const msg = "El usuario fue creado correctamente";
                swal({
                    title: 'Información',
                    text: msg,
                    icon: 'success',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-primary',
                            closeModal: true

                        }
                    }
                });
                setUsuario({
                    nombres: '',
                    email: '',
                    password: '',
                    confirmar: '',
                })
            }
        }

    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearCuenta();

    }

    return (
        <section className="ftco-section">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 text-center mb-5">
                        <h2 className="heading-section">Bienvenido</h2>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-12 col-lg-16">
                        <div className="wrap d-md-flex">
                            <div className="img" style={{ backgroundImage: 'url(https://i.ibb.co/dLHcXgG/Registrate.png)' }}>
                            </div>
                            <div className="login-wrap p-4 p-md-5">
                                <div className="d-flex">
                                    <div className="w-100">
                                        <h3 className="mb-4">Crear Cuenta</h3>
                                    </div>
                                </div>
                                <form onSubmit={onSubmit} action="#" className="signin-form">
                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="name">Nombres</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombres"
                                            name="nombres"
                                            placeholder="Nombres"
                                            value={nombres}
                                            onChange={onChange}
                                            required />
                                    </div>
                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="password">Email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Email"
                                            value={email}
                                            onChange={onChange}
                                            required />
                                    </div>

                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="password">Contraseña</label>
                                        <input type="password"
                                            className="form-control"
                                            id="password"
                                            name="password"
                                            placeholder="Contraseña"
                                            value={password}
                                            onChange={onChange}
                                            required />
                                    </div>

                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="password">Confirmar Contraseña</label>
                                        <input type="password"
                                            className="form-control"
                                            id="confirmar"
                                            name="confirmar"
                                            placeholder="Confirmar Contraseña"
                                            value={confirmar}
                                            onChange={onChange}
                                            required />
                                    </div>

                                    <div className="col-12">
                                        <button type="submit" className="btn btn-primary btn-block">Crear Cuenta</button>
                                    </div>
                                    <p>
                                    </p><div className="col-12">
                                        <Link to={"/Login"} className="btn btn-block btn-danger">
                                            Regresar al Login
                                        </Link>
                                    </div>
                                    <p />
                                    <p class="text-center">Regresar al <Link data-toggle="tab" to={"/"}>Inicio</Link></p>
                                </form></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    );
}

export default CrearCuenta;
import React from 'react';
import { Link } from 'react-router-dom';




const Inicio = () => {
    return (
        <><nav className="navbar navbar-expand-lg bg-light">
            <div className="container-fluid">
                <img src="https://i.ibb.co/Sm43K6s/logopet.png" alt="" width="50" height="50" />
                <Link className="navbar-brand" href="#">Omega Pet Shop</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" href="#">Inicio</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" href="#">Quienes Somos</Link>
                        </li>
                        <li className="nav-item dropdown">
                            <Link className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Ingresar
                            </Link>
                            <ul className="dropdown-menu">
                                <li><Link to={"/Login"} className="dropdown-item">Login</Link></li>
                                <li><Link to={"/crear-cuenta"} className="dropdown-item">Crear Cuenta</Link></li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link disabled">Disabled</Link>
                        </li>
                    </ul>

                  
                    <form class="d-flex">
                        <button class="btn btn-outline-dark" type="submit">
                        <i class="bi-cart-fill me-1"></i>
                            Carrito
                            <span class="badge bg-dark text-white ms-1 rounded-pill">0</span>
                        </button>
                    </form>
                </div>
            </div>
        </nav><div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
                <div className="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={0} className="active" aria-current="true" aria-label="Slide 1" />
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={1} aria-label="Slide 2" />
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={2} aria-label="Slide 3" />
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={3} aria-label="Slide 4" />
                </div>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img src="https://i.ibb.co/K2rhX4w/Bienvenido.png" className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item">
                        <img src="https://i.ibb.co/D1dBh3y/fondo3.png" className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item">
                        <img src="https://i.ibb.co/SdX43T8/fondo2.png" className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item">
                        <img src="https://i.ibb.co/k5706W4/fondo1.png" className="d-block w-100" alt="..." />
                    </div>
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true" />
                    <span className="visually-hidden">Anterior</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true" />
                    <span className="visually-hidden">Siguiente</span>
                </button>
            </div>

            

            <section className="py-5">
                <div className="container px-4 px-lg-5 mt-5">
                <h1>Productos</h1>
                    <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/9Hx5Zhp/bolsos-cargadores.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Bolso Cargador</h5>
                                            <h6>Diferentes diseños de Bolsos para transportar a tus mascotas.</h6>
                                        {/* Product price*/}
                                        $120.000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/xMWj65C/toallitas-humedas.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Toallitas Humedas</h5>
                                            <h6>Paquete con 40 toallitas humedas para tus peluditos</h6>
                                        $5000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/vXWC01R/plato.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Plato para mascotas</h5>
                                            <h6>Plato para mascotas con base pequeÑo</h6>
                                        $25.000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link  className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/ySJMMpZ/pelota-goma-arcoiris.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Pelota de goma Arcoiris</h5>
                                            <h6>Pelota redonda de goma para perros y gatos, juguete interactivo para masticar, con diseño de arcoíris de colores</h6>
                                        {/* Product price*/}
                                        $4000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link  className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/Jnj89WZ/llaveros.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Placas de identificación</h5>
                                            <h6>Diferentes estilos de placas para tus mascotas</h6>
                                        $8.000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link  className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/1zGQRxh/juguetes-gatos.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Set juguetes para gatos</h5>
                                            <h6>Juguetes varios</h6>
                                        {/* Product price*/}
                                        $3.500
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link  className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/Qf0mw27/juego-gato.png" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Juguete para gatos</h5>
                                            <h6>Este producto es ideal para los gatos jueguen de una manera mas dinámica y no sufran estreses o irritación</h6>
                                        {/* Product price*/}
                                        $45.000
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                        <div className="col mb-5">
                            <div className="card h-100">
                                {/* Product image*/}
                                <img className="card-img-top" src="https://i.ibb.co/VLRJq7T/champu.jpg" alt="..." />
                                {/* Product details*/}
                                <div className="card-body p-4">
                                    <div className="text-center">
                                        {/* Product name*/}
                                        <h5 className="fw-bolder">Champu Repelente</h5>
                                            <h6>Para perros y gatos, protege de pulgas, garrapatas y mosquitos</h6>
                                        {/* Product price*/}
                                        $40.00
                                    </div>
                                </div>
                                {/* Product actions*/}
                                <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div className="text-center"><Link  className="btn btn-outline-dark mt-auto" href="#">Añadir al Carrito</Link ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            
        <footer class="py-5 bg-primary">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Omega Pet Shop 2022</p></div>
        </footer>

        </>

    );
}


export default Inicio;
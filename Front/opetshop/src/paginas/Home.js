import React from "react";
import ContentHeader from "../componentes/ContentHeader";
import Navbar from "../componentes/Navbar";
import SiderbarContainer from "../componentes/SidebarContainer";
import Footer from "../componentes/Footer";
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SiderbarContainer></SiderbarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Dashboard"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Dashboard"}
                    ruta={"/home"}
                />
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">

                            <div className="col-lg-3 col-6">
                                <div className="small-box bg-info">
                                <div className="inner">
                                    <h3>Productos</h3>
                                    <p>&nbsp;</p>
                                </div>
                                <div className="icon">
                                    <i className="fa fa-edit" />
                                </div>
                                <Link to={"/productos-pet"} className="small-box-footer">Ver Prductos <i className="fas fa-arrow-circle-right" /></Link>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>

            </div>
            <Footer></Footer>
        </div>
    )
}



export default Home;
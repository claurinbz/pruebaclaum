import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert';

const Login = () => {
    //para redireccionar de un componentea otro
    const navigate = useNavigate ();
    
    //se define el estado inicial de las variables
    const [usuario, setUsuario] = useState({
        email: '',
        password: ''
    });

    const  { email, password} = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        document.getElementById("email").focus();
    }, [])


    const iniciarSesion = async () => {
        if (password.length < 6){
            const msg = "La contraseña deber de 6 caracteres";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }else {
            const data = {
                email: usuario.email,
                password: usuario.password
            }
            const response = await APIInvoke.invokePOST(`/api/auth`, data); // api/usuarios es la direccion que esta en el insomnia ejemplo localhost4000/api/usuario
            const mensaje = response.msg;

            if (mensaje === 'El usuario no existe' || mensaje === 'Contraseña incorrecta'){
                const msg = "No fue posible iniciar sesión, verificar los datos ingresados";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

            }else{
                //obtener el token de acceso
                const jwt = response.token;
                
                //guardar el token en localstorage
                localStorage.setItem('token', jwt);

                //se redirecciona al componente home
               navigate("/home");

            }
        }
    }


    const onSubmit = (e) => {
       e.preventDefault();
       iniciarSesion();

    }




    return (
        <section className="ftco-section">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 text-center mb-5">
                        <h2 className="heading-section">Bienvenido</h2>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-md-12 col-lg-10">
                        <div className="wrap d-md-flex">
                            <div className="img" style={{ backgroundImage: 'url(https://i.ibb.co/9WhwVN1/logologin.png)' }}>
                            </div>
                            <div className="login-wrap p-4 p-md-5">
                                <div className="d-flex">
                                    <div className="w-100">
                                        <h3 className="mb-4">Iniciar Sesión</h3>
                                    </div>
                                </div>
                                <form onSubmit={onSubmit}action="#" className="signin-form">
                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="name">Usuario</label>
                                        <input type="email" 
                                            className="form-control" 
                                            id="email"
                                            name="email"
                                            placeholder="email" 
                                            value={email}
                                            onChange={onChange}
                                            required />
                                    </div>
                                    <div className="form-group mb-3">
                                        <label className="label" htmlFor="password">Contraseña</label>
                                        <input type="password" 
                                            className="form-control" 
                                            id="password"
                                            name="password"
                                            placeholder="Contraseña" 
                                            value={password}
                                            onChange={onChange}
                                            required />
                                    </div>
                                    <div className="col-12">
                                        <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
                                    </div>
                                    <p>
                                    </p><div className="col-12">
                                        <Link to={"/crear-cuenta"} className="btn btn-block btn-danger">
                                            Crear Cuenta
                                        </Link>
                                    </div>
                                    <p />
                                    <p className="text-center">Regresar al <Link data-toggle="tab" to={"/"}>Inicio</Link></p>
                                    
                                </form></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    );
}

export default Login;
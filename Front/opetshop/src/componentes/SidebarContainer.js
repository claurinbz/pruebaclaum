import React from 'react';
import { Link } from 'react-router-dom';
import Menu from './Menu';

const SiderbarContainer = () => {
  return (
    <aside className="main-sidebar sidebar-light-primary elevation-4">
      <div className="brand-link">
        <img src="https://i.ibb.co/T07mG7Y/Icono.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
        <span className="brand-text font-weight-light">Omega Pet Shop</span>
      </div>
      <div className="sidebar">
        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
          <div className="info">
            &nbsp;
          </div>
          <div className="info">
            <Link to={"/home"} className="d-block">Menu Principal</Link>
          </div>
        </div>
        <Menu></Menu>
      </div>
    </aside>

  );
}

export default SiderbarContainer;
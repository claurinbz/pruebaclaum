//este archivo es el administrador de las rutas.
//es el que va a permitir ir de una pagina a otra o de un componente a otro.  
import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom'; //estos componentes se van a usar para el roteo
import Inicio from './paginas/Inicio';
import Login from './paginas/autenticacion/Login';
import CrearCuenta from './paginas/autenticacion/CrearCuenta';
import Home from './paginas/Home';
import ProductosPet from './paginas/Productos/ProductosPet';

//los nombre de las rutas deben ser diferentes a los nombre de los componentes ejemplo
// componente CrearCuenta, la ruta es crear-cuenta

function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
        <Route path="/" exact element={<Inicio/>}/>
          <Route path="/Login" exact element={<Login/>}/>
          <Route path="/crear-cuenta" exact element={<CrearCuenta/>}/>
          <Route path="/home" exact element={<Home/>}/>
          <Route path="/productos-pet" exact element={<ProductosPet/>}/>
          
              
      
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
